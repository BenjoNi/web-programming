<!DOCTYPE html>
<head lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Kuki Car Parts</title>
</head>
<body>
<link href="css/bootstrap.min.css" rel="stylesheet">
<div class="container">
  <h1 class="text-center">Kuki Car Parts</h1>
<p>&nbsp;</p>
<?php
echo "Order recieved at: " . date("Y-m-d h:i:sa");
?>
<?php
define("Tiresprice", 100);

define("Oilprice", 10);

define("Sparksprice", 4);
?>

<?php
$tiresquantity= $_POST['tiresquantity'];
$oilquantity= $_POST['oilquantity'];
$sparkquantity= $_POST['sparkquantity'];
?>

<p>Your order is as follows:</p>

<p>
<?php
echo "Items ordered:",($tiresquantity + $oilquantity + $sparkquantity);
?>
</p>

<table class="table">
  <thead class="thead-inverse">
    <tr>
     
      <th>Item</th>
      <th>Quantity</th>
      <th>Price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Tires</th>
      <td><?php echo $tiresquantity; ?></td>
      <td><?php echo $tiresquantity * Tiresprice ?></td>
     
    </tr>
    <tr>
      <th scope="row">Oil</th>
      <td><?php echo $oilquantity; ?></td>
      <td><?php echo $oilquantity * Oilprice; ?></td>
     
    </tr>
    <tr>
      <th scope="row">Spark Plugs</th>
      <td><?php echo $sparkquantity; ?></td>
      <td><?php echo $sparkquantity * Sparksprice; ?></td>
    </tr>
  </tbody>
</table>
<p>&nbsp;</p>
<?php
$Subtotal = ($tiresquantity* Tiresprice)+($oilquantity * Oilprice)+($sparkquantity * Sparksprice);
?>
<?php
echo "Total without tax:", $Subtotal;
?>
<p>
<?php
echo "Total with tax:", ($Subtotal*(110/100)) 
?>
</p>
<?php
 
 $selectOption = $_POST['taskOption'];
?>


<?php
echo "Customer found us: ", $selectOption;
?>

</p>
</html>